var Handlebars = require('handlebars');
exports = module.exports = function(api){
    this.api = api;
    this.run = function(params, device) {
        console.log("run ", params, device);
        var imageName = 'picture.jpg';
        var user = this.api.getContacts()[params[0]];
        var email = user.email;
        var text = Handlebars.compile('{{name}} is {{status}}')(device);
        console.log('text', text);
        console.log('take picture');
        this.api.takePictureFoscam(imageName, function(imagePath){
            console.log("emailing pcture",imagePath);
            this.api.email(email, "Attention: "+text, text, imageName, imagePath);
        }.bind(this));
    };
};
