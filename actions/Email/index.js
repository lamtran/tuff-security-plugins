var Handlebars = require('handlebars');
exports = module.exports = function(api){
    this.api = api;
    this.run = function(params, device) {
        console.log("run ", params, device);
        var user = this.api.getContacts()[params[0]];
        var email = user.email;
        var text = Handlebars.compile('{{name}} is {{status}}')(device);
        console.log('emailing ' + email + "text: ", text);
        this.api.email(email, "Attention: "+text, text);
    };
};
