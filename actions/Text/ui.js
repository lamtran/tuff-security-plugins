define(function(require){
    'use strict';
    var ContactsPicker  = require('shared/ui/ContactsPicker');

    var Ui = function(server, pluginId){
        ContactsPicker.call(this, {
            server : server,
            filters: ['phone'],
            pluginId : pluginId
        });
    };

    Ui.prototype = Object.create(ContactsPicker.prototype);
    Ui.prototype.constructor = Ui;

    return Ui;
});
