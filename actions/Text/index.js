var Handlebars = require('handlebars');

var Plugin = function(api){
    this.api = api;
    this.run = function(params, device) {
        console.log("run ", params, device);
        var user = this.api.getContacts()[params[0]];
        var phone = user.phone;
        if(phone.indexOf("+1")!==0) {
            phone = "+1" + phone;
        }
        var text = Handlebars.compile('{{name}} is {{status}}')(device);
        this.api.sms(phone, text);
    };
};

exports = module.exports = Plugin;
