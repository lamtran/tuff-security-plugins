var Handlebars = require('handlebars');
exports = module.exports = function(api){
    this.api = api;
    this.run = function(params, device) {
        console.log("run ", params, device);
        var text = Handlebars.compile(params[0].template)(device);
        this.api.notify(text);
    };
};
