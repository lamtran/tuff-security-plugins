define(function(require){
    'use strict';
    var MultipleChoicePicker  = require('shared/ui/MultipleChoicePicker');

    var Ui = function(server, pluginId){
        MultipleChoicePicker.call(this, {
            server: server,
            pluginId : pluginId
        });
    };

    Ui.prototype = Object.create(MultipleChoicePicker.prototype);
    Ui.prototype.constructor = Ui;

    return Ui;
});
