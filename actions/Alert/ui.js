define(function(require){
    'use strict';
    var defaultTemplatePath    = require.toUrl('./configure.html');
    var PluginHelper    = require('views/helpers/PluginHelper');
    var _           = require('underscore');

    var defaultTitle = 'Alert';

    var ContactsPicker = function(server, pluginId){
        this.server = server;
        this.templatePath = defaultTemplatePath;//config.templatePath ? config.templatePath : defaultTemplatePath;
        this.filters = [];//config.filters ? config.filters : [];
        this.title = defaultTitle;//config.title ? config.title : defaultTitle;
        this.pluginId = pluginId;//config.pluginId ? config.pluginId : '';
    };

    ContactsPicker.prototype.edit = function(dialog, device, userSelections, armed){
        var promise = $.Deferred();
        console.log(device, userSelections);
        require(['text!'+this.templatePath],function(tmpl){
            var template = _.template( tmpl, {
                device : device,
                armed : armed
            });
            dialog.getBody().html(template);
            dialog.getHeader().html(this.title);
            var saveBtn = $('<button type="button" class="btn btn-primary btn-lg" data-btn-type="save">Save</button>');
            var cancelBtn = $('<button type="button" class="btn btn-primary btn-lg">Cancel</button>');
            var deleteBtn = $('<button type="button" class="btn btn-primary btn-lg">Delete Action</button>');
            saveBtn.on('click',function(){
//                var contactsSelected = dialog.getBody().find(':checkbox:checked').map(function(idx, el) {
//                    return Number(el.id);
//                });
                var data = ['message to primary responders', 'message to secondary responders', 'message to call center'];//contactsSelected.toArray();
                if($('#armed-checkbox').is(':checked')){
                    data.armed = true;
                }
                promise.resolve(data);
            });

            cancelBtn.on('click',function(){
                promise.resolve(-1);
            });
            deleteBtn.on('click',function(){
                promise.reject();
            });
            dialog.getFooter().append(deleteBtn);
            dialog.getFooter().append(cancelBtn);
//            dialog.getFooter().append(saveBtn);
        }.bind(this))
        /*
        this.server.getPlugins().done(function(plugins) {
            console.log("got plugins", plugins);
            var chainPromise = PluginHelper.editAction(dialog, plugins[0].id, plugins[0].ui, userSelections[0], device.name, device.status);
            var results = [];
            if(plugins.length>1) {
                for(var i=1;i<plugins.length;i++) {
                    chainPromise = chainPromise.then(function(ii){
                        return function(result){
                            results.push(result);
                            return PluginHelper.editAction(dialog, plugins[ii].id, plugins[ii].ui, userSelections[ii], device.name, device.status);
                        };
                    }(i));
                }
            }
            chainPromise.done(function(result){
                results.push(result);
                console.log("all configured", results);
                results = results.map(function(result, index) {
                    if(result === -1) {
                        return userSelections[index]
                    }else{
                        return result;
                    }
                });
                promise.resolve(results);
            });
            chainPromise.fail(promise.reject);
            chainPromise.always(function(){
                console.log('promise always called!');
                $('#overlay').modal('hide');
            });
        });
        */
        return promise;
    };
    ContactsPicker.prototype.configure = function(dialog, device, buttons){
        var promise = $.Deferred();
        console.log('device', device);
        require(['text!'+this.templatePath],function(tmpl){
            var template = _.template( tmpl, {
                device : device,
                armed : true
            });
            dialog.getBody().html(template);
            dialog.getHeader().html(this.title);
            var saveBtn = $('<button type="button" class="btn btn-primary btn-lg" data-btn-type="save">'+ ((buttons && buttons.save) ? buttons.save : 'Add') + '</button>');
            var cancelBtn = $('<button type="button" class="btn btn-primary btn-lg" data-btn-type="save">'+ ((buttons && buttons.cancel) ? buttons.cancel : 'Cancel') + '</button>');
            saveBtn.on('click',function(){
                var data = ['message to primary responders', 'message to secondary responders', 'message to call center'];//contactsSelected.toArray();
                if($('#armed-checkbox').is(':checked')){
                    data.armed = true;
                }
                promise.resolve(data);
            });
            cancelBtn.on('click',function(){
                promise.reject();
            });
            dialog.getFooter().append(cancelBtn);
            dialog.getFooter().append(saveBtn);
        }.bind(this));
        /*
        this.server.getPlugins().done(function(plugins) {
            console.log("got plugins", plugins);
            var chainPromise = PluginHelper.configureAction(plugins[0].id, plugins[0].ui, device);
            var results = [];
            if(plugins.length>1) {
                for(var i=1;i<plugins.length;i++) {
                    chainPromise = chainPromise.then(function(ii){
                        return function(result){
                            results.push(result);
                            return PluginHelper.configureAction(plugins[ii].id, plugins[ii].ui, device)
                        };
                    }(i));
                }
            }
            chainPromise.done(function(result){
                results.push(result);
                console.log("all configured", results);
                promise.resolve(results);
            });
            chainPromise.always(function(){
                console.log('promise always called!');
                $('#overlay').modal('hide');
            });
        });
        */
        return promise;
    };

    return ContactsPicker;
});
