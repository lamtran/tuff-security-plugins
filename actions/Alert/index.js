var when = require('when');

var PRIMARY_RESPONDERS = 0;
var SECONDARY_RESPONDERS = 1;

var Plugin = function(api){
    this.api = api;
    this.run = function(params, device) {
        console.log("run alert ", params, device);
        var system = this.api;
        if(this.api.isSystemArmed()) {
            console.log("DO SOMETHING!!");
            var promise = callUsers(PRIMARY_RESPONDERS,system, true);

            promise.then(function(results) {
                console.log("ALL CALLED", results);
                if(!Array.isArray(results)){
                    results = [results];
                }

                var filter = results.filter(function(result) {
                    return result.enteredCancelCode;
                });
                if(filter.length) {
                    console.log("cancel code was entered, disarm");
                    system.disarm();
                }else{
                    console.log("cancel code was not entered, escalate");
                    try{
                        system.panic({pluginId:'Alert'});
                    }catch(e){
                        console.log("exceptionin panic", e);
                    }
                    console.log("call secondary responsders");
                    var secondPromise = callUsers(SECONDARY_RESPONDERS,system, false, 'Security Alarm triggered.  Please check on Thanh and Lam immediately!');
                    secondPromise.always(function(){
                        if(system.isSystemArmed()) {
                            console.log("SECONDARY CALLERS are contacted, and system is still not disarmed, super escalate");
                            callCallCenter();
                        }
                    })
                }
            },function(){
                console.log("CALL FAILED");
            });
            promise.always(function(){
                console.log("ALL CALLS ARE MADE");
            });
        }

        // provision api to allow running the plugin this plugin "uses"
        // get all plugins involved
        // run them
    };
    var formatPhoneNumber = function(phone) {
        if(phone.indexOf("+1")!==0) {
            phone = "+1" + phone;
        }
        return phone;
    };

    var callUsers = function(group, system, withCancel, message) {
        var users = system.getContactsByGroup(group);
        console.log("USFRS in group: ", users);
        var firstUser = users[0];
        console.log("calling first user", firstUser.name, firstUser.phone);
        var returnPromise = when.defer();
        var promise
        if(withCancel){
            promise = system.callWithCancel(formatPhoneNumber(firstUser.phone));
        }else{
            promise = system.call(formatPhoneNumber(firstUser.phone), message);
        }
        var called = [];
        var api = system;
        if(users.length > 1) {
            for(var i= 1,j=users.length;i<j;i++) {
                promise = promise.then(function(ii){
                    return function(result){
                        called.push(result);
                        if(!result.enteredCancelCode) {
                            console.log("calling next user", formatPhoneNumber(users[ii].phone));
                            if(withCancel) {
                                return api.callWithCancel(formatPhoneNumber(users[ii].phone));
                            }else{
                                return api.call(formatPhoneNumber(users[ii].phone),message);
                            }
                        }
                    }
                }(i));
            }
        }
        promise.then(function(results){
            called.push(results);
            returnPromise.resolve(results);
        }, function(error){
            called.push(error);
            returnPromise.reject(called);
        });
        return returnPromise.promise;
    };
    var callCallCenter = function(){
        console.log("CALL CALL CENTER NOW!!!!!!!!!!!!!!!!!!!");
    }
};

exports = module.exports = Plugin;
