var Handlebars = require('handlebars');

var Plugin = function(api){
    this.api = api;
    this.run = function(params, device, withCancel) {
        console.log("run ", params, device);
        var user = this.api.getContacts()[params[0]];
        var phone = user.phone;
        if(phone.indexOf("+1")!==0) {
            phone = "+1" + phone;
        }
        var promise;
        if(withCancel!==undefined && withCancel === true){
            promise = this.api.call(phone);
        }else{
            var text = Handlebars.compile('{{name}} is {{status}}')(device);
            promise = this.api.call(phone, text);
        }
        return promise;
    };
};

exports = module.exports = Plugin;
