define(function(require){
    'use strict';
    var DevicesPicker  = require('shared/ui/DevicesPicker');

    var Ui = function(server, pluginId){
        DevicesPicker.call(this, {
            server: server,
            filters: ['actionable'],
            pluginId : pluginId
        });
    };

    Ui.prototype = Object.create(DevicesPicker.prototype);
    Ui.prototype.constructor = Ui;

    return Ui;
});
